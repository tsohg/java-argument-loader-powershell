#! /usr/bin/pwsh

#Path to current folder
$PATH_FOLDER=(Get-Location)

. $PATH_FOLDER/config.ps1


$JAVA_CMD=(which java)
$JAVA_DIR=(Split-Path -Path $JAVA_CMD)
$JAVA_NAME=(Split-Path -Path $JAVA_CMD -Leaf -Resolve)
$JAVA_BACKUP_CMD=(Join-Path -Path $JAVA_DIR -ChildPath $JAVA_BACKUP_NAME)


if((Test-Path  $JAVA_BACKUP_CMD)){
    Write-Host "Already installed"
    Exit
}

#Backup the original command
Move-Item -Path $JAVA_CMD -Destination $JAVA_BACKUP_CMD

#Install the wrapper script
Copy-Item -Force $JAVA_WRAPPER -Destination $JAVA_CMD

#Update the path variable in the wrapper
cat $JAVA_WRAPPER | % { $_ -replace "path_folder_place_holder","$PATH_FOLDER" }  > $JAVA_CMD 
