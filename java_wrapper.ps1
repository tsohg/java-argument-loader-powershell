#! /usr/bin/pwsh

#Source config file
$PATH_SOURCE="/home/karaoui/dev/freelance/deepak"
$CONFIG_NAME="config.ps1"
$CONFIG_PATH=(Join-Path -Path $PATH_SOURCE -ChildPath $CONFIG_NAME)
. $CONFIG_PATH

#Get arguments
$JAVA_WRAPPER_ARGS_PATH=(Join-Path -Path $PATH_SOURCE -ChildPath $JAVA_WRAPPER_ARGS)
$ADDED_ARGS=(cat $JAVA_WRAPPER_ARGS_PATH)

#For debug only
Write-Host "Actual command executed java  $ADDED_ARGS $args"

#Execute the command
$JAVA_BACKUP_CMD=(which $JAVA_BACKUP_NAME)
Start-Process -NoNewWindow -FilePath "$JAVA_BACKUP_CMD" -ArgumentList "$ADDED_ARGS $args"
