#! /usr/bin/pwsh


#Path to current folder
$PATH_FOLDER=(Get-Location)
. $PATH_FOLDER/config.ps1

#Path to current folder
$JAVA_CMD=(which java)
$JAVA_DIR=(Split-Path -Path $JAVA_CMD)
$JAVA_NAME=(Split-Path -Path $JAVA_CMD -Leaf -Resolve)
$JAVA_BACKUP_CMD=(Join-Path -Path $JAVA_DIR -ChildPath $JAVA_BACKUP_NAME)


if(-Not (Test-Path  $JAVA_BACKUP_CMD)){
    Write-Host "Nothing to uninstall"
    Exit
}

#Backup the original command
Move-Item -Force -Path $JAVA_BACKUP_CMD -Destination $JAVA_CMD
